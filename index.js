const handlebars = require("handlebars");
const { parseISO, format } = require('date-fns');
const { template, styles } = require("./output.js");

function render(input) {
  const _template = handlebars.compile(template);
  return _template({ ...input, styles }, {
    helpers: {
      'formatDate': (dateStr) => {
        const date = parseISO(dateStr);

        let formatStr = 'yyyy';
        if (dateStr.length > 4) {
          formatStr = 'MM/yyyy'
        }

        return format(date, formatStr);
      }
    }
  });
}

module.exports = { render };
