# JSON Resume bhdouglass Theme

This is a theme for [JSON resume](https://jsonresume.org/): an awesome open source project for generating uniform resumes, with free hosting and customizable themes.

This is a fork of the [JSON Resume Minyma Theme](https://github.com/godraadam/jsonresume-theme-minyma)
 
## Useage
 
Check out my [JSON resume tutorial](https://bhdouglass.com/blog/how-to-build-a-developer-json-resume/)
for how to setup your own resume, then use this theme as show below:


``` 
npm i jsonresume-theme-bhdouglass
resume export resume.html --theme bhdouglass
```
 
## Screenshots

![screenshot](screenshots/screenshot.png)
 
## Developing

Make changes to `src/input.css` and/or `src/template.hbs`
and run `npm run watch` to have it automatically compile.
